# Source Code And Build
Both the zipped source code and the Windows build of the game can be found under in the [Releases](https://gitlab.com/agddjumpkings/jumpking/-/releases/v1.0.0)

## Statement of Design Goals
### Team Bounce Queens

#### List your design goals for your game.
 
We wanted to make a fairly simple and accessible game that would appeal to a wide audience. With that in mind we used a fairy tale/royalty theme that people are already well accustomed to. 
 
We drew a lot of inspiration from jump king and played around with that idea to make a new character that bounces off walls using her heavy set body and strong legs. We felt like it sounded like good fun and wanted to try making a slightly different main character than you usually see in video games.
 
While the premise and theme isn't complicated it is designed to be challenging. Like in jumpking you can fall down a long way before you come to a stop and the player has no control over the character while falling, so this might be a bit frustrating. It might not be the most enjoyable experience for very inexperienced platformer game players but for those players we try to help them out by giving them the option to use the checkpoints and powerslider.
 
The art style is not complex at all but we tried to make it endearing and lovable by using a cartoon style and funny facial expressions.
 
#### If your team pursued any experimental features for your game, describe them here as well.
 
No real experimental features we would say, we instead went for really strong fundamentals and level design instead of focusing on adding features throughout the whole process. We got this advice from our instructor early on and we do not regret listening to it. 
 
All of us had the experience in the three week course that we made a game with too big of a scope and made the project so much harder technically than it needed to be to make a solid game. So this time we opted to avoid that and focus more on design.

## Things we used that we didn’t make ourselves:

Level background: [https://craftpix.net/product/jumping-up-game-backgrounds](https://craftpix.net/product/jumping-up-game-backgrounds/)

Checkpoint sound: [https://freesound.org/people/Vicces1212/sounds/123751](https://freesound.org/people/Vicces1212/sounds/123751/)

Keyprompts in main menu: [https://thoseawesomeguys.com/prompts](https://thoseawesomeguys.com/prompts/)
