using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(AudioSource))]
public class Checkpoint : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player") &&
            col.gameObject.GetComponent<PlayerController>().lastCheckpointHeight < transform.position.y)
        {
            PlayerController controller = col.gameObject.GetComponent<PlayerController>();
            controller.lastCheckpointHeight = transform.position.y;
            controller.startingPosition = transform.position;
            controller.startingCamera = controller.currentCamera;
            transform.Find("Flag").GetComponent<SpriteRenderer>().color = Color.green;
            GetComponent<AudioSource>().Play();
        }

    }
}
