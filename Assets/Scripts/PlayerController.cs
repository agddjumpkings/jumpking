using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;
using Vector2 = UnityEngine.Vector2;
using btype;
using Quaternion = UnityEngine.Quaternion;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class PlayerController : MonoBehaviour
{
    public uint inputQueueDelayMS = 0;
    public float movingSpeed = 100f;
    public float fallingSpeed = 1f;
    public float jumpStep = 1f;
    public float jumpTimeStep = 2f;
    public float jumpForce = 25f;
    public float maxJumpForce = 20f;
    public float maxVelocity = 1000f;
    public PhysicsMaterial2D bouncyMat;
    public TextMeshProUGUI jumpForceText;
    public Camera currentCamera;
    AudioSource landingSound;
    public AudioSource jumpSound;
    [HideInInspector] public Camera nextCamera;
    [HideInInspector] public float lastCheckpointHeight = float.MinValue;
    [HideInInspector] public Vector2 startingPosition;
    [HideInInspector] public Camera startingCamera;
    [HideInInspector] public Rigidbody2D body;
    [HideInInspector] public HitCheck groundCheck;
    private HitCheck _leftCheck;
    private HitCheck _rightCheck;
    private HitCheck _topCheck;
    public float _jumpHold = 0f;
    private bool _releaseJump = false;
    private bool _jumping = false;
    private bool _inAir = false;
    private Vector2 _movingVelocity = Vector2.zero;
    private Timer leftUp;
    private Timer rightUp;
    public static PlayerController instance;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        landingSound = GetComponent<AudioSource>();
        groundCheck = transform.Find("GroundCheck").GetComponent<HitCheck>();
        _leftCheck = transform.Find("LeftCheck").GetComponent<HitCheck>();
        _rightCheck = transform.Find("RightCheck").GetComponent<HitCheck>();
        _topCheck = transform.Find("TopCheck").GetComponent<HitCheck>();
        startingPosition = transform.position;
        startingCamera = currentCamera;
        leftUp = new Timer(0);
        rightUp = new Timer(0);
    }
    
    private void Update()
    {
        if (_inAir && groundCheck.grounded) {
            landingSound.Play();
            _inAir = false;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            transform.position = startingPosition;
            nextCamera = startingCamera;
            SwapCamera();
            StopJumping();
            _movingVelocity = Vector2.zero;
        }
        if(Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow))
            leftUp.Start(inputQueueDelayMS);
        if(Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow))
            rightUp.Start(inputQueueDelayMS);
        _releaseJump = Input.GetKeyUp(KeyCode.Space) || _releaseJump;
        jumpForceText.text = _jumpHold.ToString();
    }

    void FixedUpdate()
    {
        ClampVelocity();
        if (groundCheck.grounded)
            GroundedBehaviour();
        else
            AirBehaviour();
        body.velocity = _movingVelocity * Time.fixedDeltaTime;
    }
    
    public void SwapCamera()
    {
        currentCamera.enabled = false;
        Camera cam = currentCamera;
        currentCamera = nextCamera;
        currentCamera.enabled = true;
        nextCamera = cam;
    }
    
    private bool LeftHit() { return _leftCheck.grounded && body.velocity.x < 0; }
    
    private bool RightHit() { return _rightCheck.grounded && body.velocity.x > 0; }

    private bool TopHit() { return _topCheck.grounded && body.velocity.y > 0; }

    public void HitDiagonal(Vector2 direction, float boost)
    {
        _movingVelocity = direction;
        StartJumping(boost, false);
    }
    
    private void StartJumping(float boost = 0, bool getMove = true)
    {
        _movingVelocity += Vector2.up;
        if(getMove) GetMovement(true);
        _movingVelocity *= jumpForce;
        _releaseJump = false;
        body.sharedMaterial = bouncyMat;
        _jumping = true;
        _jumpHold += boost;
    }

    private void StopJumping()
    {
        _jumpHold = 0;
        body.sharedMaterial = null;
        _jumping = false;
        _releaseJump = false;
    }

    private void GetMovement(bool checkDelay = false)
    {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow) || (checkDelay && !leftUp.Passed()))
            _movingVelocity += Vector2.left;
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow) || (checkDelay && !rightUp.Passed()))
            _movingVelocity += Vector2.right;
    }

    private void CalcMovement()
    {
        GetMovement();
        _movingVelocity *= movingSpeed;
    }

    void ClampVelocity()
    {
        if (_movingVelocity.x > maxVelocity) _movingVelocity.x = maxVelocity;
        else if (_movingVelocity.x < -maxVelocity) _movingVelocity.x = -maxVelocity;
        if (_movingVelocity.y > maxVelocity) _movingVelocity.y = maxVelocity;
        else if (_movingVelocity.y < -maxVelocity) _movingVelocity.y = -maxVelocity;
    }

    void GroundedBehaviour()
    {
        
        _movingVelocity = Vector2.zero;
        if (_jumping)
        {
            StopJumping();
        }
        if (Input.GetKey(KeyCode.Space) && _jumpHold < maxJumpForce)
        {
            _jumpHold += jumpStep * Time.fixedDeltaTime;
        }
        else if (_jumpHold >= maxJumpForce || _releaseJump)
        {
            
            jumpSound.Play();
            StartJumping();
        }
        else if(!(Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)))
        {
            CalcMovement();
        }
    }
    
    void AirBehaviour()
    {
        
        _releaseJump = false;
        _inAir = true;
        if (LeftHit() || RightHit())
        {
            _movingVelocity.x *= -1;
        }
        if (TopHit())
        {
            _movingVelocity.y *= -1;
        }
        if(!_jumping)
        {
            _movingVelocity += Vector2.down * fallingSpeed;
        }
        else
        {
            _jumpHold -= jumpTimeStep * Time.fixedDeltaTime;
            if (_jumpHold <= 0)
            {
                StopJumping();
            }
        }
    }
}


