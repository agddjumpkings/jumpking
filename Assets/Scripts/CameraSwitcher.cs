using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Camera))]
public class CameraSwitcher : MonoBehaviour
{
    private Camera _camera;

    private void Start()
    {
        _camera = GetComponent<Camera>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            col.gameObject.GetComponent<PlayerController>().nextCamera = _camera;
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            PlayerController controller = col.gameObject.GetComponent<PlayerController>();
            if (controller.nextCamera == _camera)
                controller.nextCamera = _camera;
            else if (controller.currentCamera == _camera)
                controller.SwapCamera();
        }
    }
}
