using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    private bool on = true;
    void Start()
    {
        Cursor.lockState = CursorLockMode.None; 
        Cursor.visible = true;
        PlayerPrefs.SetInt("checks", 1);
    }
    public void StartGame()
    {
        SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void Quit() 
    {
        Application.Quit();
    }

    public void ToggleCheckpoints()
    {
        on = !on;
        PlayerPrefs.SetInt("checks", on ? 1 : 0);
    }
}
