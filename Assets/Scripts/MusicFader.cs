using System.Collections;
using System.Collections.Generic;
using btype;
using UnityEngine;

public class MusicFader : MonoBehaviour
{
    public float maxVolume = 1;
    public uint fadeTimeMS = 5000;
    public List<AudioSource> sources;
    public List<float> heights;
    public GameObject player;
    private Timer fadeTimer;
    private int fadeInIndex = 0;
    private int fadeOutIndex = -1;
    
    // Start is called before the first frame update
    void Start()
    {
        foreach (AudioSource source in sources)
            source.volume = 0f;
        sources[0].volume = maxVolume;
        fadeTimer = new Timer(0);
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = heights.Count-1; i >= 0; i--)
        {
            if (player.transform.position.y >= heights[i])
            {
                if (i != fadeInIndex)
                {
                    fadeOutIndex = fadeInIndex;
                    fadeInIndex = i;
                    fadeTimer.Start(fadeTimeMS);
                }
                break;
            }
        }

        if (!fadeTimer.Passed())
        {
            sources[fadeInIndex].volume = Mathf.Clamp(fadeTimer.Progress() / 100f, 0f, maxVolume);
            sources[fadeOutIndex].volume = maxVolume - Mathf.Clamp(fadeTimer.Progress() / 100f, 0f, maxVolume);
        }

        for (int i = 0; i < heights.Count; i++)
            if (i != fadeInIndex && i != fadeOutIndex)
                sources[i].volume = 0f;
    }
}
