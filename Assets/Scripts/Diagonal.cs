using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Diagonal : MonoBehaviour
{
    public float boost = 1f;
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            Vector2 dir = transform.lossyScale.x < 0 ? Vector2.left : Vector2.right;
            dir += transform.lossyScale.y < 0 ? Vector2.down * 2 : Vector2.zero;
            col.GetComponent<PlayerController>().HitDiagonal(dir, boost);
        }
    }
}
