using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EasyToggle : MonoBehaviour
{
    public List<GameObject> easyObjects;
    public bool status = false;
    public TextMeshProUGUI info;
    public HitCheck stopTimeCheck;
    private bool _stopTime = false;

    private void UpdateStatus()
    {
        foreach (GameObject g in easyObjects)
        {
            g.SetActive(status);
        }
    }

    private void Start()
    {
        status = PlayerPrefs.GetInt("checks") == 1;
        UpdateStatus();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            status = !status;
            UpdateStatus();
        }
        if (!_stopTime)
            info.text = Time.time.ToString("0.00");
        if (stopTimeCheck.grounded)
            _stopTime = true;
    }
}
