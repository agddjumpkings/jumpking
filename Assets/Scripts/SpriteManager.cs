using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteManager : MonoBehaviour
{
    public Sprite idle;
    public Sprite walk;
    public Sprite charge;
    public Sprite chargeSide;
    public Sprite up;
    public Sprite down;
    public PlayerController controller;
    private SpriteRenderer _renderer;
    private bool lastLookedRight = true;

    private void Start()
    {
        _renderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (controller.groundCheck.grounded)
        {
            int direction = 0;
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
                direction -= 1;
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
                direction += 1;
            if (direction >= 0 && lastLookedRight)
                _renderer.flipX = false;
            else
                _renderer.flipX = true;
            if (direction < 0) lastLookedRight = false;
            else if (direction > 0) lastLookedRight = true;
            if(!Input.GetKey(KeyCode.Space))
            {
                if (direction != 0 && !Input.GetKey(KeyCode.S))
                    _renderer.sprite = walk;
                else
                    _renderer.sprite = idle;
            }
            else
            {
                if (direction != 0)
                    _renderer.sprite = chargeSide;
                else
                    _renderer.sprite = charge;
            }
        }
        else
        {
            if (controller.body.velocity.x >= 0)
                _renderer.flipX = false;
            else
                _renderer.flipX = true;
            if (controller.body.velocity.x > 0) lastLookedRight = true;
            else if (controller.body.velocity.x < 0) lastLookedRight = false;
            if (controller.body.velocity.y >= 0)
                _renderer.sprite = up;
            else
                _renderer.sprite = down;
        }

    }
}
