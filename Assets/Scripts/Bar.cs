using System.Collections;
using System.Collections.Generic;
using btype;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class Bar : MonoBehaviour
{
    public Slider Slider;
    public Color Low;
    public Color High;
    public Vector3 Offset;
    public uint fadeOutMS = 500;
    public uint fadeInMS = 125;
    private Timer _fadeTimer;
    private CanvasGroup _canvasGroup;
    private bool _fadeOut = true;

    public void Awake()
    {
        Offset = new Vector3(0,1,0);
        Slider.maxValue = 3.0f;
        _fadeTimer = new Timer();
        _canvasGroup = GetComponent<CanvasGroup>();
        _canvasGroup.alpha = 0;
    }

    void Update()
    {
        Slider.transform.position = Camera.main.WorldToScreenPoint(transform.parent.position + Offset);

        Slider.value = PlayerController.instance._jumpHold;
        Slider.fillRect.GetComponentInChildren<Image>().color = Color.Lerp(Low, High, Slider.normalizedValue);

        if (Slider.value == 0 && _canvasGroup.alpha != 0)
        {
            if (!_fadeOut)
            {
                _fadeOut = true;
                _fadeTimer.Start(fadeOutMS);
            }
            else
            {
                _canvasGroup.alpha *= 1f - Mathf.Clamp(_fadeTimer.Progress() / 100f, 0f, 1f);
            }
        }
        else if (Slider.value != 0 && _canvasGroup.alpha == 0)
        {
            if (_fadeOut)
            {
                _fadeOut = false;
                _fadeTimer.Start(fadeInMS);
            }
        }
        if(!_fadeOut)
        {
            _canvasGroup.alpha = Mathf.Clamp(_fadeTimer.Progress() / 100f, 0f, 1f);
        }
    }


}
